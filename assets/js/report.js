var Report = function() {
    var $profile = $('[name="profile"]'),
        $dates_wrapper = $('#daterange'),
        $date1 = $dates_wrapper.find('input:first'),
        $date2 = $dates_wrapper.find('input:last'),
        $campaigns_wrapper = $('#campains'),
        $graph = $('#graph'),
        $report_type_wrapper = $('#select-report-type'),
        graph = null,
        current_query = {},
        campaign = {
            name: '',
            id: null,
            type: 0
        },
        profileId = $profile.val(),
        global_segment = 'sessions::condition::ga:sourceMedium==google / cpc',
        global_filters = 'ga:sourceMedium==google / cpc';

    var init = function() {
        var d = new Date();
        $('.input-daterange').find('input:last').val(d.yyyymmdd());
        d.setDate(d.getDate() - 1);
        $('.input-daterange').find('input:first').val(d.yyyymmdd());

        $('.input-daterange').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
        });

        $graph.highcharts({
            chart: {
                type: 'line'
            },
            yAxis: {
                title: {
                    text: 'CPL'
                },
            },
            xAxis: {
                title: {
                    text: 'Datetime'
                },
                type: 'datetime'
            },
            title: {
                text: 'CPL'
            },
            legend: {
                enabled: false
            },
            series: [{
                name: 'CPL',
                data: []
            }]
        });
        graph = $graph.highcharts();

        $('[data="no-data-alert"]').hide();
        $graph.hide();
        $('.dates').hide();
    };
    var reloadSidebar = function() {
        queryCoreReportingApi({
                'metrics': 'ga:sessions',
                'dimensions': 'ga:campaign,ga:adwordsCampaignID',
                'filters': global_filters
            },
            handleAdWordsCampaignsList,
            true);
    };
    var handleAdWordsCampaignsList = function(results) {
        $loading.hide();
        if (results.totalResults) {
            var cmpg = '';

            results.rows.unshift(['Все кампании AdWords', 0]);
            for (var i = 0; i < results.rows.length; i++) {
                var active = results.rows[i][1] == campaign.id ? 'active' : '';
                cmpg += '<li class="' + active + '"><a href="#" data-campaign-id="' + results.rows[i][1] + '">' + results.rows[i][0] + '</a></li>';
            };
            $campaigns_wrapper.html(cmpg);
            $('.dates').show();
        }
    };
    var queryCoreReportingApi = function(query, callback, skip) {
        if (profileId) {
            $loading.show();
            var q = {
                'ids': 'ga:' + profileId,
                'start-date': $date1.val(),
                'end-date': $date2.val(),
            };

            if (skip) {
                $.extend(q, query);
            } else {
                current_query = $.extend(q, query);
            }

            console.log('Querying Core Reporting API. Query:');
            console.log(q);

            gapi.client.analytics.data.ga.get(q).execute(callback);
        } else {
            console.log('No profile id');
        }
    };
    var gaApiGetCampaignInfo = function() {
        $('[data="no-data-alert"]').hide();

        var q = {
            'metrics': 'ga:sessions,ga:goal4Completions,ga:goal4ConversionRate,ga:goal1Completions,ga:goal1ConversionRate,ga:adCost',
            'dimensions': (campaign.type === 1 ? 'ga:keyword' : 'ga:date'), // keywords or date types
            'filters': (campaign.id ? 'ga:adwordsCampaignID==' + campaign.id : global_filters) // filter by camapaigID if we have id
        };
        if (campaign.id !== null) {
            // get campaigns data
            queryCoreReportingApi(q, printTable);
            // 7 and 30 days
            var cpl_7 = Cookies.get('cpl_7'),
                cpl_30 = Cookies.get('cpl_30');
            if (!cpl_7 || !cpl_30) {
                q['start-date'] = '30daysAgo';
                q['end-date'] = 'yesterday';
                queryCoreReportingApi(q, saveCpl7And30, true);
            } else {
                $('#cpl7').text(cpl_7);
                $('#cpl30').text(cpl_30);
                $('#cpls').show();
            }
        } else {
            $('[data-alert="no-campaings"]').show();
        }
    };
    var saveCpl7And30 = function(results) {
        if (results.totalResults) {
            var last_day = results.rows.length - 1,
                cpl_7 = 0, cpl_30 = 0, cost = 0, goals = 0;
            for (var i = last_day, j = 0; i >= 0; i--, j++) {
                var adCost = parseFloat(results.rows[i][6]),
                    goal1Completions = parseFloat(results.rows[i][4]),
                    goal2Completions = parseFloat(results.rows[i][2]);

                cost += adCost;
                goals += (goal2Completions + goal1Completions);
                if (j == 6) {
                    cpl_7 = cost / goals;
                }
                if (j == 29) {
                    cpl_30 = cost / goals;
                }
            };
            cpl_7 = cpl_7 == Infinity ? 'N/A' : Math.round(cpl_7 * 100) / 100;
            cpl_30 = cpl_30 == Infinity ? 'N/A' : Math.round(cpl_30 * 100) / 100;
            Cookies.set('cpl_7', cpl_7);
            Cookies.set('cpl_30', cpl_30);
            $('#cpl7').text(cpl_7);
            $('#cpl30').text(cpl_30);
            $('#cpls').show();
        }
    }
    var printTable = function(results) {
        $loading.hide();
        var source = $("#all-campaigns-table").html(),
            template = Handlebars.compile(source),
            cpl = {
                days: 0,
                na: 0,
                total: 0,
                goals: 0,
                cost: 0,
                data: [],
                title: campaign.name
            };

        if (results.totalResults) {
            var last_day = results.rows.length - 1;

            for (var i = last_day, j = 0; i >= 0; i--, j++) {
                var adCost = parseFloat(results.rows[i][6]),
                    goal1Completions = parseFloat(results.rows[i][4]),
                    goal2Completions = parseFloat(results.rows[i][2]),
                    date_str = results.rows[i][0];

                date_str = [date_str.substr(0, 4), date_str.substr(4, 2), date_str.substr(6)];
                results.rows[i][0] = date_str[0] + '-' + date_str[1] + '-' + date_str[2];

                if (goal2Completions || goal1Completions) {
                    cpl.total += results.rows[i][7] = adCost / (goal2Completions + goal1Completions);
                    cpl.goals += (goal2Completions + goal1Completions);
                    results.rows[i][7] = Math.round(results.rows[i][7] * 100) / 100;
                    cpl.data.push([Date.UTC(date_str[0], date_str[1], date_str[2]), results.rows[i][7]]);
                } else {
                    results.rows[i][7] = 'N/A';
                    cpl.na++;
                }
                if (adCost || goal2Completions || goal1Completions) {
                    cpl.cost += adCost;
                    cpl.days++;
                }

                results.rows[i][3] = Math.round(results.rows[i][3] * 100) / 100 + '%';
                results.rows[i][5] = Math.round(results.rows[i][5] * 100) / 100 + '%';
            };
            cpl.total = Math.round((cpl.cost / cpl.goals) * 100) / 100;
            $('#cpltotal').text(cpl.total);
            if (campaign.type === 0) {
                plotGraph(cpl)
            } else {
                $graph.hide();
            };
        }

        results.totalsForAllResults.dates = $date1.val() + ' - ' + $date2.val();
        results.totalsForAllResults.adCost = results.totalsForAllResults['ga:adCost'];
        results.totalsForAllResults.goal1ConversionRate = Math.round(results.totalsForAllResults['ga:goal1ConversionRate'] * 100) / 100 + '%';
        results.totalsForAllResults.goal1Completions = results.totalsForAllResults['ga:goal1Completions'];
        results.totalsForAllResults.goal4ConversionRate = Math.round(results.totalsForAllResults['ga:goal4ConversionRate'] * 100) / 100 + '%';
        results.totalsForAllResults.goal4Completions = results.totalsForAllResults['ga:goal4Completions'];
        results.totalsForAllResults.sessions = results.totalsForAllResults['ga:sessions'];
        results.totalsForAllResults.cpl = cpl.total;

        var context = {
            sumrow: results.totalsForAllResults,
            rows: results.rows,
            rows_count: results.totalResults,
            cpl: cpl,
            campaign: campaign
        };
        $('#table-wraper').html(template(context));
        console.log(results);
    };
    var plotGraph = function(data) {
        // if (data.na < data.data.length * 0.75) {
        graph.series[0].update({
            data: data.data
        });
        graph.setTitle({
            text: data.title
        });
        $('[data-alert="no-graph"]').hide(function() {
            $graph.show();
        });
        // } else {
        //     $('[data-alert="no-graph"]').show(function(){
        //         $graph.hide();
        //     });
        // }
    };

    init();

    $profile.on('change gaApiGetAdWordsCampaignsList.km', function() {
        profileId = $profile.val();
        reloadSidebar();
        $('[data="no-data-alert"]').hide();
        // $('body').trigger('gaApiGetCampaignInfo.km');
    });
    $campaigns_wrapper.on('click', '[data-campaign-id]', function(e) {
        var $this = $(this),
            $wrap = $this.closest('li');

        campaign.id = $this.data('campaign-id');
        campaign.name = $this.text();

        $wrap.addClass('active').siblings().removeClass('active');

        $('body').trigger('gaApiGetCampaignInfo.km');
    })
    $dates_wrapper.on('changeDate', function(ev) {
        reloadSidebar(); // campaigns list changed
        $('body').trigger('gaApiGetCampaignInfo.km');
    });
    $report_type_wrapper.on('click', '[data-campaign-report]', function(e) {
        e.preventDefault();
        var $this = $(this);

        $report_type_wrapper.find('[data-campaign-report]').toggleClass('btn-primary');
        campaign.type = $this.data('campaign-report');
        $('body').trigger('gaApiGetCampaignInfo.km');
    })
    $('body').on('gaApiGetCampaignInfo.km', function(e) {
        e.preventDefault();

        gaApiGetCampaignInfo(campaign.id);
    })
};
