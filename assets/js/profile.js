var Profiles = function() {
    var $account = $('[name="account"]'),
        $property = $('[name="property"]'),
        $profile = $('[name="profile"]');

    var getProfiles = function() {
        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().execute(handleAccounts);
    };
    var handleAccounts = function(results) {
        if (!results.code) {
            if (results && results.items && results.items.length) {
                // fill select with accounts
                var options = "";
                for (var i = results.items.length - 1; i >= 0; i--) {
                    options += '<option value="' + results.items[i].id + '">' + results.items[i].name + '</option>';
                };
                $account.html(options)
                    .val(results.items[0].id)
                    .trigger('change');
                $('.selectpicker').selectpicker('refresh');
            } else {
                console.log('No accounts found for this user.')
            }
        } else {
            console.log('There was an error querying accounts: ' + results.message);
        }
    };
    var queryWebproperties = function(accountId) {
        console.log('Querying Webproperties.');

        // Get a list of all the Web Properties for the account
        gapi.client.analytics.management.webproperties.list({
            'accountId': accountId
        }).execute(handleWebproperties);
    };
    var handleWebproperties = function(results) {
        if (!results.code) {
            if (results && results.items && results.items.length) {
                // fill select with webproperties
                // if only one - triger select
                var options = "";
                for (var i = results.items.length - 1; i >= 0; i--) {
                    options += '<option value="' + results.items[i].id + '">' + results.items[i].name + '</option>';
                };
                $property.html(options)
                    .val(results.items[0].id)
                    .trigger('change');
                $('.selectpicker').selectpicker('refresh');
            } else {
                console.log('No webproperties found for this user.');
            }
        } else {
            console.log('There was an error querying webproperties: ' + results.message);
        }
    };
    var queryProfiles = function(accountId, webpropertyId) {
        console.log('Querying Views (Profiles).');

        // Get a list of all Views (Profiles) for the first Web Property of the first Account
        gapi.client.analytics.management.profiles.list({
            'accountId': accountId,
            'webPropertyId': webpropertyId
        }).execute(handleProfiles);
    };
    var handleProfiles = function(results) {
        if (!results.code) {
            if (results && results.items && results.items.length) {
                // fill select with profiles
                // if only one - triger select
                var options = "";
                for (var i = results.items.length - 1; i >= 0; i--) {
                    options += '<option value="' + results.items[i].id + '">' + results.items[i].name + '</option>';
                };
                $profile.html(options).val(results.items[0].id).trigger('change');
                $('.selectpicker').selectpicker('refresh');
            } else {
                console.log('No views (profiles) found for this user.');
            }
        } else {
            console.log('There was an error querying views (profiles): ' + results.message);
        }
    };
    $account.on('change', function(e) {
        $property.empty();
        $profile.empty();

        queryWebproperties($(this).val());
    });
    $property.on('change', function(e) {
        $profile.empty();

        queryProfiles($account.val(), $(this).val());
    });

    getProfiles();
};