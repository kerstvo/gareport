// This function is called after the Client Library has finished loading
function handleClientLoad() {
    new GaAuth();
}

var GaAuth = function() {
    var clientId = '109928778386-tlnq98cf9m5ku556sq8h80aobuugh7r8.apps.googleusercontent.com',
        apiKey = 'AIzaSyADSYFntSrE1F-IhPDWwYsTG9bWsojYRzU',
        scopes = 'https://www.googleapis.com/auth/analytics.readonly';


    var init = function() {
        $('[data-action="authorize"]').hide();
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth, 1);
    };
    var checkAuth = function() {
        gapi.auth.authorize({
            client_id: clientId,
            scope: scopes,
            immediate: true
        }, handleAuthResult);
    };
    var handleAuthResult = function(authResult) {
        if (authResult && !authResult.error) {
            // The user has authorized access
            // Load the Analytics Client. This function is defined in the next section.
            loadAnalyticsClient();
        } else {
            console.log(authResult.error);
            // User has not Authenticated and Authorized
            handleUnAuthorized();
        }
    };
    // Authorized user
    var handleAuthorized = function() {
        $('[data-action="authorize"]').closest('nav').hide();
        $('body').trigger('gaApiAuthorizes.km')
    };
    // Unauthorized user
    var handleUnAuthorized = function() {
        $('.loading').hide();
        $('[data-action="apitest"]').hide();
        $('[data-action="authorize"]').on('click', function(e) {
            e.preventDefault();

            gapi.auth.authorize({
                client_id: clientId,
                scope: scopes,
                immediate: false
            }, handleAuthResult);
            return false;
        }).closest('nav').show();
        $('[data-action="authorize"]').show();

    };
    var loadAnalyticsClient = function() {
        // Load the Analytics client and set handleAuthorized as the callback function
        gapi.client.load('analytics', 'v3', handleAuthorized);
    };
    init();
};
