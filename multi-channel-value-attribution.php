<?php
set_time_limit(900);

// user settings
$VIEW_ID = '103571440';
$sessionCount = 7;
$shoppingStages = array('ADD_TO_CART', 'CHECKOUT', 'TRANSACTION');
$date1 = '2015-07-01';
$date2 = '2015-08-31';


// mode
$v = isset($_GET['v']) ? $_GET['v'] : 0;

// Load the Google API PHP Client Library.
require_once './google-api-php-client/src/Google/autoload.php';

// Start a session to persist credentials.
session_start();

// Create the client object and set the authorization configuration
// from the client_secretes.json you downloaded from the developer console.
$client = new Google_Client();
$client->setAuthConfigFile(
    'client_secret_109928778386-1obf4s6efqbheuca0acvroqho29oja7h.apps.googleusercontent.com.json'
);
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

// If the user has already authorized this app then get an access token
// else redirect to ask the user to authorize access to Google Analytics.
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
    // Set the access token on the client.
    $client->setAccessToken($_SESSION['access_token']);

    // Create an authorized analytics service object.
    /***
     * @var Google_Service_Analytics
     */
    $analytics = new Google_Service_Analytics($client);


    $client->setUseBatch(true);
    $batch = new Google_Http_Batch($client);
    // Get the results from the Core Reporting API and print the results.
    $results = array();
    $queries = array();
    $sequencs = array('^ga:source==google');
    for ($i = 1; $i <= $sessionCount; $i++) {
        $sequencs[] = 'ga:source==google';
        $options = array(
            'filters' => $v == 0 ? 'ga:source==google;ga:sessionCount==' . $i : 'ga:source==google',
            'dimensions' => 'ga:shoppingStage',
            'segment' => $v == 1 ? 'users::condition::ga:sessionCount==' . $i : ($v == 2 ? 'users::sequence::' . join(
                    ';->',
                    $sequencs
                ) : '')
        );
        if (!$v) {
            unset($options['segment']);
        }
//        $results[$i] = $analytics->data_ga->get(
        $request = $analytics->data_ga->get(
            'ga:' . $VIEW_ID,
            $date1,
            $date2,
            'ga:users',
            $options
        );
        $queries[] = array(
            'ids' => 'ga:' . $VIEW_ID,
            'startDate' => $date1,
            'endDate' => $date2,
            'metrics' => 'ga:users',
            'options' => $options
        );
        $batch->add($request, $i);
    }
    $results = $batch->execute();
    // Print results
    $table = array();
    foreach ($results as $key => $value) {
        /**
         * @var $value Google_Service_Analytics_GaData
         */
        try {
            if (count($value->getRows()) > 0) {

                // Get the entry for the first entry in the first row.
                $rows = $value->getRows();
                foreach ($rows as $key_row => $val_row) {
                    if (in_array($val_row[0], $shoppingStages)) {
                        $table[str_replace('response-', '', $key)][$val_row[0]] = $val_row[1];
                    }
                }

            }
        } catch (Google_Exception $e) {
            die($e->getMessage());
        }
    }

} else {
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php';
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>multi-channel-value-attribution</title>
    <meta charset="UTF-8">
    <meta name=description content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <style>
        .container > div {
            padding-bottom: 20px;
        }
    </style>
</head>
<body>
<h1 class="text-center">Mig Cigs</h1>

<div class="container">
    <div class="col-xs-12">
        <ul class="nav nav-pills">
            <li <?php if ($v == 0): ?>class="active"<?php endif ?>><a href="?v=0">Use filter ga:sessionCount</a></li>
            <li <?php if ($v == 1): ?>class="active"<?php endif ?>><a
                    href="?v=1">Use segment condition ga:sessionCount</a></li>
            <li <?php if ($v == 2): ?>class="active"<?php endif ?>><a
                    href="?v=2">Use segment sequence ga:sessionCount</a></li>
        </ul>
    </div>
    <div class="col-xs-12">
        <table class="table table-hover">
            <thead>
            <tr>
                <th></th>
                <th>Add to cart</th>
                <th>Checkout</th>
                <th>Transaction</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($table as $key => $value): ?>
                <tr>
                    <td>Session <?= $key ?></td>
                    <td><?= isset($value['ADD_TO_CART']) ? $value['ADD_TO_CART'] : '0' ?></td>
                    <td><?= isset($value['CHECKOUT']) ? $value['CHECKOUT'] : '0' ?></td>
                    <td><?= isset($value['TRANSACTION']) ? $value['TRANSACTION'] : '0' ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-xs-12">
        <div class="well well-sm">
            <h3>All queries</h3>
            <pre><?php print_r($queries); ?></pre>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>
</html>
